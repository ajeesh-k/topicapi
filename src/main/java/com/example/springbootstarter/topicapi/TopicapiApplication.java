package com.example.springbootstarter.topicapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopicapiApplication.class, args);
	}

}
