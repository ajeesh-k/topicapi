package com.example.springbootstarter.topicapi.controller;

import com.example.springbootstarter.topicapi.entity.Topic;
import com.example.springbootstarter.topicapi.exception.TopicNotFoundException;
import com.example.springbootstarter.topicapi.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class TopicController {

    @Autowired
    private TopicService topicService;


    /**
     * Controller method to get all topics.
     * @return all entities
     */
    @GetMapping("/topics")
    public ResponseEntity<List<Topic>> getAllTopics(){
        List<Topic> allTopics = topicService.getAllTopics();
    return new ResponseEntity<>(allTopics, HttpStatus.OK);
    }


    /**
     *  To get entity (topic) by id.
     * @param id Id of entity requested by user.
     * @return Optional of an entity.
     */
    @GetMapping("/topics/{id}")
    public ResponseEntity<Topic> getTopicById (@PathVariable String id ) throws TopicNotFoundException {
        Topic topicToSent = topicService.getTopic(id);
        return new ResponseEntity<>(topicToSent,HttpStatus.OK);
    }

    /**
     * Controller method to save an entity to system.
     * @param topic an entity to save.
     * @return "Topic added".
     */
    @PostMapping("/topics")
    public String addTopic(@Valid @RequestBody Topic topic){
        topicService.addTopic(topic);
        return "Topic added";
    }


    /**
     * To update an entity.
     * @param topic Topic entity to update.
     * @param id id of topic entity.
     * @return "Topic updated".
     */
    @PutMapping("/topics/{id}")
    public String updateTopic(@RequestBody Topic topic, @PathVariable String id) {
        topicService.updateTopic(topic,id);
        return "Topic "+id+" updated";
    }


    /**
     * Controller method to deletes a topic entity.
     * @param id Id of entity to be deleted.
     * @return "Topic deleted"
     */
    @DeleteMapping("/topics/{id}")
    public String deleteTopic( @PathVariable String id){
        topicService.deleteTopic(id);
        return "Topic "+id+" deleted";
    }
}
