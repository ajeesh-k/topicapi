package com.example.springbootstarter.topicapi.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Topic {

    @Id
    @NotBlank(message = "ID cannot be blank")
    @NotNull
    private String id;

    @NotNull
    @NotBlank(message = "Name cannot be blank!")
    private String name;

    private String description;

}
