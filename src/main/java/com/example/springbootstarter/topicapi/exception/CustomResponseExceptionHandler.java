package com.example.springbootstarter.topicapi.exception;

import com.example.springbootstarter.topicapi.entity.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(TopicNotFoundException.class)
    public ResponseEntity<ErrorMessage> handleTopicNotFoundException(TopicNotFoundException exception) {
        List<String> details = new ArrayList<>();
        details.add(exception.getMessage());
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND,details);
        return new ResponseEntity<>(errorMessage,HttpStatus.NOT_FOUND);
    }
}
