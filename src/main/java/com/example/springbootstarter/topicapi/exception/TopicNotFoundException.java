package com.example.springbootstarter.topicapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class TopicNotFoundException extends RuntimeException{

    public TopicNotFoundException(String message) {
        super(message);
    }
}
