package com.example.springbootstarter.topicapi.repository;

import com.example.springbootstarter.topicapi.entity.Topic;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TopicDataRepo extends JpaRepository<Topic, String> {
}