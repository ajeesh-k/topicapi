package com.example.springbootstarter.topicapi.service;

import com.example.springbootstarter.topicapi.entity.Topic;
import com.example.springbootstarter.topicapi.exception.TopicNotFoundException;
import com.example.springbootstarter.topicapi.repository.TopicDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class TopicService {

    @Autowired
    private TopicDataRepo topicDataRepo;

    /**
     * To get all topics.
     * @return all entities
     */
    public List<Topic> getAllTopics() {
        return topicDataRepo.findAll();
    }

    /**
     *  Controller method to get entity (topic) by id.
     * @param id Id of entity requested by user.
     * @return Optional of an entity.
     */
    public Topic getTopic(String id) {
        Optional<Topic> topicFromDB = topicDataRepo.findById(id);
        if (!(topicFromDB.isPresent())) {
            throw new TopicNotFoundException("Topic Not Found");
        }
        return topicFromDB.get();
    }

    /**
     * To save an entity to system.
     * @param topic an entity to save.
     */
    public void addTopic(Topic topic) {
        topicDataRepo.save(topic);
    }

    /**
     * To update an entity.
     * @param topic Topic entity to update.
     * @param id id of topic entity.
     */
    public void updateTopic(Topic topic, String id) {
        topicDataRepo.save(topic);
    }

    /**
     * Deletes a topic entity.
     * @param id Id of entity to be deleted.
     */
    public void deleteTopic(String id){
        topicDataRepo.deleteById(id);
    }

}
